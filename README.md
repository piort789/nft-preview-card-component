# Frontend Mentor - NFT preview card component solution

This is a solution to the [NFT preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)




### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements

### Screenshot

![](./screenshot.jpg)


### Links

- Solution URL: [https://gitlab.com/piort789/nft-preview-card-component](https://gitlab.com/piort789/nft-preview-card-component)
- Live Site URL: [https://piort789.gitlab.io/nft-preview-card-component/](https://piort789.gitlab.io/nft-preview-card-component/)


### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow



### What I learned

- how to overlay images on top of each other
- how to set difrent level of opacity on parent element and child element

### Useful resources

- [How To Create Image Overlay Hover Effect Using Only HTML & CSS](https://www.youtube.com/watch?v=Himo9n0BaDw) - This helped me for XYZ reason. I really liked this pattern and will use it going forward.




## Author

- GitLab - [Piotr Juszczak](https://gitlab.com/piort789)
- Frontend Mentor - [@Pio678](https://www.frontendmentor.io/profile/Pio678)



